#NF26 Project

##Question 1

###Generation :

getWeatherByTime(nom_de_fichier, param_limite)  

Pour ne pas mettre de limite, entrer 0 dans param_limite  

###Obtention de courbes

getHistoryByStation(station, parameter) > Génère une image PNG dans /home/dbermond/  

station = nom de la station en 4 caractères
parameter = indicateur désiré

Indicateurs autorisés : temperature, dew point, humidity, wind speed, precipitation, visibility, feel.

##Question 2

###Generation :

getWeatherQ2(nom_de_fichier, param_limite)  

Pour ne pas mettre de limite, entrer 0 dans param_limite  

###Obtention de carte

getMapQ2(indicateur, heure, jour, mois, année) > Génère une image PNG dans /home/dbermond/Outputs/ finissant par le timestamp de l'éxecution  

Indicateurs autorisés (insensible à la casse) : longitude, latitude, temperature, dewpoint, relativehumidity, windspeed, precipitation, visibility, apparenttemperature.
