import matplotlib.pyplot as plt
from cassandra.cluster import Cluster

#Connexion au cluster

cluster = Cluster(['localhost'])
session = cluster.connect('dbermond_projet')

# Equivalence entre les mots-clé et leur place dans la table
dictParameter = {
    "temperature": 13,
    "dew point": 6,
    "humidity": 11,
    "wind speed": 12,
    "precipitation": 10,
    "visibility": 14,

    "feel": 7,
}

#Récupération des information pour une station à une date donnée

def getDataByStationAndDate(station, year, month):
    query = '''
        SELECT * FROM weatherbylocation WHERE station = '%s' AND valid_year = %d AND valid_month = %d ;
        '''
    resultat = session.execute(query % (station, year, month))
    return resultat

#Calcul de la moyenne pour un paramétre donné
def getMean(result, parameter):
    n = 0
    mean = 0
    c = dictParameter[parameter]
    for row in result:
        if (row[c] != None):
            n = n + 1
            mean += (row[c] - mean)/n
    
    return mean

#Dessin des courbes
def getHistoryByStation(station, parameter):
    months = [1,2,3,4,5,6,7,8,9,10,11,12]
    year = 2010
    meanSeason = [0,0,0,0,0,0,0,0,0,0,0,0,]

    for i in range(0,3):
        year += 1
        month = 0
        listMean = []
        for j in range(0,12):
            month += 1
            try :
                result = getDataByStationAndDate(station, year, month)
            except:
                print("Erreur lors de la récupération des données")
            if result != None:
                mean = getMean(result, parameter)
                listMean.append(mean)                       #Stock la moyenne de chaque mois dans une liste
                meanSeason[j] += mean                       #Variable intermédiraire pour calculer la moyenne par mois sur plusieurs années
        p = plt.plot(months, listMean, label=str(year))     #Traçage de la courbe pour une année

    for k in range(0,12):
        meanSeason[k] = meanSeason[k]/3

    plt.ylabel(parameter)
    plt.xlabel("Mois")
    plt.plot(months, meanSeason, label='Moyenne')           #Traçage de la courbe moyenne
    plt.legend()
    plt.savefig("courbeMean.png")                           #Sauvegarde des courbes
    plt.clf()
    return meanSeason