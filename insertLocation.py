import csv
import re

def getData(container, value, type):
    result = container[value]
    if (result == "M" or result == "T") :
        result = None

    if (type == "string"):
        return result
    elif (type == "float") :
        if (result != None):
            result = float(result)
        return result
      
    elif (type == "date") :
        if (result != None) :
            dateparser = re.compile("(?P<year>\d+)-(?P<month>\d+)-(?P<day>\d+) (?P<hour>\d+):(?P<minute>\d+)")
            match = dateparser.match(container[value])
            if not match :
                return "continue"
            data = match.groupdict()
            result = (
                int(data["year"]),
                int(data["month"]),
                int(data["day"]),
                int(data["hour"]),
                int(data["minute"]),
            )
            return result
    else :
        return None

def loadataLocation(filename): 
    with open(filename) as f:
        for r in csv.DictReader(f):
            timestamp = getData(r, "valid", "date")
            if (timestamp == "continue" or timestamp == None):
                continue
            data = {}
            data["valid"] = timestamp
            data["station"] = getData(r, "station", "string")

            data["lon"] = getData(r, "lon", "float")
            data["lat"] = getData(r, "lat", "float")
            data["tmpc"] = getData(r, "tmpc", "float")
            data["dwpc"] = getData(r, "dwpc", "float")
            data["relh"] = getData(r, "relh", "float")
            data["sknt"] = getData(r, "sknt", "float")
            data["p01m"] = getData(r, "p01m", "float")
            data["vsby"] = getData(r, "vsby", "float")
            data["feel"] = getData(r, "feel", "float")

            yield data


import json

from cassandra.cluster import Cluster
#Connexion au cluster
cluster = Cluster(['localhost'])   
session = cluster.connect('dbermond_projet')

#Création de la table 
query = '''
    DROP TABLE IF EXISTS weatherByLocation ;
    '''

session.execute(query)

query = '''
    CREATE TABLE weatherByLocation(
    station text,

    valid_year varint,
    valid_month varint,
    valid_day varint,
    valid_hour varint,
    valid_minute varint,

    lon float,
    lat float,
    tmpc float,
    dwpc float,
    relh float,
    sknt float,
    p01m float,
    vsby float,

    feel float,

    PRIMARY KEY ((station), valid_year, valid_month, valid_day,  valid_hour, valid_minute)
    );
'''

session.execute(query)

#Fonction de chargement des données dans la table
def getWeatherByLocation(csvfilename, session):
    data = loadataLocation(csvfilename)
    for r in data:
        t = (
            r["station"],

            r["valid"][0],
            r["valid"][1],
            r["valid"][2],
            r["valid"][3],
            r["valid"][4],

            r["lon"],
            r["lat"],
            r["tmpc"],
            r["dwpc"],
            r["relh"],
            r["sknt"],
            r["p01m"],
            r["vsby"],

            r["feel"],
            )
        query = """
        INSERT INTO weatherByLocation(

                station,

                valid_year,
                valid_month,
                valid_day,
                valid_hour,
                valid_minute,

                lon,
                lat,
                tmpc,
                dwpc,
                relh,
                sknt,
                p01m,
                vsby,
                
                feel)
        VALUES (%s, %s, %s ,%s ,%s, %s, %s, %s ,%s ,%s, %s, %s, %s, %s, %s)
        """
        session.execute(query, t)


#Chargement des données

getWeatherByLocation("/home/dbermond/Data/asos-2004>2013.txt", session)