import datetime
from cassandra.cluster import Cluster
import matplotlib.pyplot as plt
import datetime
import matplotlib as mpl
from matplotlib.colors import LinearSegmentedColormap
import matplotlib.gridspec as gridspec

def checkDate(Hour, Day, Month, Year):
    dateIsValid = True
    try :
        datetime.datetime(year = Year, month = Month, day = Day, hour = Hour)
    except ValueError :
        print("La date saisie est invalide")
        dateIsValid = False

    return dateIsValid

def checkIndicatorQ2(indicator) :
    indicatorsAllowed = ["longitude", "latitude", "temperature", "dewpoint", "relativehumidity", "windspeed", "precipitation", "visibility", "apparenttemperature"]
    if (indicator.lower() in indicatorsAllowed) :
        return True
    else :
        return False

def getStatisticsQ2(indicator, hour, day, month, year) :
    if (not(checkIndicatorQ2(indicator))) :
        print("Indicateur ("+str(indicator)+") non autorisé.")
        return False

    t = (
        year,
        month,
        day,
        hour
    )
    cluster = Cluster(['localhost'])
    session = cluster.connect('dbermond_projet')

    query = "SELECT " + indicator.lower() + " FROM weatherByTime  WHERE DhmYear = %s AND DhmMonth = %s AND DhmDay = %s AND DhmHour = %s "
    #print("Query = "+query)
    results = session.execute(query, t)
    nQ2 = 0
    meanQ2 = 0
    sdQ2 = 0
    sumQ2 = 0
    sumQ22 = 0
    minQ2 = 99999999999999
    maxQ2 = -9999999999999
    for row in results:
        if (row[0] == None) :
            continue
        nQ2 += 1
        if (row[0] > maxQ2):
            maxQ2 = row[0]
        if (row[0] < minQ2):
            minQ2 = row[0]
        meanQ2 += (row[0] - meanQ2) / nQ2
        sumQ2 += row[0]
        sumQ22 += row[0] ** 2
    if (nQ2 == 0):
        print("Aucune donnée pour la date saisie (" + str(day) + "/" + str(month) + "/" + str(year) + ").")
    else:
        sdQ2 = (sumQ22 / nQ2 - (sumQ2 / nQ2) ** 2) ** (1 / 2)
        return (nQ2, meanQ2, minQ2, maxQ2)

def getColorsRanges(min, max, mean, parts) : # renvoie liste de [Valeur [Rouge, Vert, Bleu]]
    """ Linear """

    colorValues = [min + (x/(parts - 1)) * (max - min) for x in range(parts)]
    colorRanges = []

    for i in range(parts) :
        oneColor = [0, [0, 0, 0]]                                       # [Valeur [Rouge, Vert, Bleu]]
        oneColor[0] = colorValues[i]
        if (i < parts / 2) :
            oneColor[1][0] = 1.0                                        # Rouge
            oneColor[1][1] = i / (parts / 2)                            # Vert
        else :
            oneColor[1][0] = 1.0 - ((i - parts / 2) / (parts-1))        # Rouge
            oneColor[1][1] = 1.0                                        # Vert
        colorRanges.append(oneColor)

    return colorRanges


    """ Mean as Yellow
    partsBeforeMean = int((parts - 1) / 2)
    partsAfterMean = parts - partsBeforeMean - 1
    beforeMean = [min + (x/partsBeforeMean) * (mean - min) for x in range(partsBeforeMean)]
    afterMean = [mean + ((x+1)/partsAfterMean) * (max - mean) for x in range(partsAfterMean)]
    colorValues = beforeMean + [mean] + afterMean
    colorRanges = []

    for i in range(parts) :
        oneColor = [0, [0, 0, 0]]                                                     # [Valeur [Rouge, Vert, Bleu]]
        if (i < partsBeforeMean) :
            oneColor[0] = colorValues[i]
            oneColor[1][0] = 1.0                                             # Rouge
            oneColor[1][1] = i / partsBeforeMean                             # Vert
        elif (i == partsBeforeMean) :
            oneColor[0] = colorValues[i]
            oneColor[1][0] = 1.0                                             # Rouge
            oneColor[1][1] = 1.0                                             # Vert
        else :
            oneColor[0] = colorValues[i]
            oneColor[1][0] = 1.0 - ((i - partsBeforeMean) / partsAfterMean)  # Rouge
            oneColor[1][1] = 1.0                                             # Vert
        colorRanges.append(oneColor)

    return colorRanges
    """

def getPointsAndColor(indicator, hour, day, month, year, numberOfPoint, colorRanges) : # renvoie liste [Valeur, [Latitude, Longitude], [Rouge, Vert, Bleu]]
    t = (
        year,
        month,
        day,
        hour
    )
    cluster = Cluster(['localhost'])
    session = cluster.connect('dbermond_projet')

    query = """
        SELECT
        """ + indicator.lower() + """, latitude, longitude
        FROM weatherByTime
        WHERE DhmYear = %s AND DhmMonth = %s AND DhmDay = %s AND DhmHour = %s
        """
    results = session.execute(query, t)

    points = []

    for row in results:
        #print (row)
        onePoint = [0, [0, 0], [0, 0, 0]]  # [Valeur, [Latitude, Longitude], [Rouge, Vert, Bleu]]
        if (row[0] == None) :
            continue
        onePoint[0] = row[0]                      # Valeur
        onePoint[1] = [row[1], row[2]]            # GPS
        for color in colorRanges :
            if (row[0] <= color[0]) :
                onePoint[2] = color[1]            # Couleur RVB
                break
        points.append(onePoint)

    return points

def getListsForScatter(input):
    latitudes = []
    longitudes = []
    RGBcodes = []

    for data in input :
        if (data[0] == None) :
            continue
        latitudes.append(data[1][0])
        longitudes.append(data[1][1])
        RGBcodes.append(data[2])

    return latitudes, longitudes, RGBcodes


def getColorBar(min, max) :

    colors = [(1, 0, 0), (1, 1, 0), (0, 1, 0)]   # Rouge >  Jaune > Vert
    cm = LinearSegmentedColormap.from_list("colorMap", colors, 255)
    norm = mpl.colors.Normalize(vmin = min, vmax = max)

    return cm, norm

    """ Mean as Yellow
    colorsBeforeMean = [(1, 0, 0), (1, 1, 0)]  # Rouge > Jaune
    colorsAfterMean = [(1, 1, 0), (0, 1, 0)]  # Jaune > Vert
    cmBeforeMean = LinearSegmentedColormap.from_list("cmBM", colorsBeforeMean, 255)
    cmAfterMean = LinearSegmentedColormap.from_list("cmAM", colorsAfterMean, 255)

    normBeforeMean = mpl.colors.Normalize(vmin=min, vmax=mean)
    normAfterMean = mpl.colors.Normalize(vmin=mean, vmax=max)

    return cmBeforeMean, cmAfterMean, normBeforeMean, normAfterMean
    """

"""
    colorsBeforeMean = [[],[],[]]       #R,V,B
    colorsAfterMean = [[],[],[]]
    for color in colorRanges :
        if (color[0] <= mean) :
            colorsBeforeMean[0].append(color[1][0])
            colorsBeforeMean[1].append(color[1][1])
            colorsBeforeMean[2].append(color[1][2])
        if (color[0] >= mean) :
            colorsAfterMean[0].append(color[1][0])
            colorsAfterMean[1].append(color[1][1])
            colorsAfterMean[2].append(color[1][2])


    dictBeforMean = ("red" : tuple())

    cbBeforeMean = mpl.colorbar.ColorbarBase(ax, cmap=cmap,
                                    norm=norm,
                                    orientation='horizontal')

    cbBeforeMean = mpl.colorbar.ColorbarBase(ax, cmap=cmap,
                                    norm=norm,
                                    orientation='horizontal')
"""

def getIndicatorUnit(indicator) :
    indicator = indicator.lower()
    if (indicator == "longitude"):
        return "°"
    elif (indicator == "latitude"):
        return "°"
    elif (indicator == "temperature"):
        return "°C"
    elif (indicator == "dewpoint"):
        return "°C"
    elif (indicator == "relativehumidity"):
        return "%"
    elif (indicator == "windspeed"):
        return "noeuds"
    elif (indicator == "precipitation"):
        return "mm"
    elif (indicator == "visibility"):
        return "miles"
    elif (indicator == "apparenttemperature") :
        return "°F"

def getMapQ2(indicator, hour, day, month, year) :
    if (not(checkDate(hour, day, month, year))) :
        return False
    statistics = getStatisticsQ2(indicator, hour, day, month, year)
    if (statistics == False) :

        return False
    else :
        N, Mean, Min, Max = statistics
    numberOfColors = min(N, 509)
    colorRanges = getColorsRanges(Min, Max, Mean, numberOfColors)
    points = getPointsAndColor(indicator, hour, day, month, year, N, colorRanges)
    Background = plt.imread("/home/dbermond/DEmap.png")
    fig = plt.figure(1)
    gridspec.GridSpec(1, 10)
    plt.subplot2grid((1, 10), (0, 0), colspan = 9)
    latitudes, longitudes, RGBcodes = getListsForScatter(points)
    #plt.subplot(2, 1, 1, autoscale_on = True)
    sc = plt.scatter(x = longitudes, y = latitudes, c = RGBcodes, zorder = 1)
    plt.imshow(Background, extent = [5.3,15.0,47.0,55.0], zorder = 0)
    inputTime = datetime.datetime(year = year, month = month, day = day, hour = hour).strftime("%d-%m-%Y-%Hh")
    unit = getIndicatorUnit(indicator)
    plt.title(indicator + " (en " + unit + ") pour le " + inputTime)
    plt.xlabel("Longitude")
    plt.ylabel("Latitude")
    #ax = plt.subplot(2, 1, 2, autoscale_on = True)
    ax = plt.subplot2grid((1, 10), (0, 9), colspan=1)
    cmap, norm = getColorBar(Min, Max)
    mpl.colorbar.ColorbarBase(ax, cmap=cmap,
                                norm=norm,
                                orientation='vertical')
    plt.show()
    """ Mean as Yellow
    colorMapBeforeMean, colorMapAfterMean, normBeforeMean, normAfterMean = getColorBar(Min, Max, Mean)
    mpl.colorbar.ColorbarBase(ax1, cmap=colorMapBeforeMean,
                              norm=normBeforeMean,
                              orientation='vertical')
    mpl.colorbar.ColorbarBase(ax2, cmap=colorMapAfterMean,
                              norm=normAfterMean,
                              orientation='vertical')
    """
    currentTime = datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
    outputName = "/home/dbermond/Outputs/mapFrom" + inputTime + "--" + currentTime + ".png"
    plt.savefig(outputName)
    return '"'+outputName+'" créé'


''' ********************************** Fonctions abandonnées *********************************************************
def computeQuantile(indicator, hour, day, month, year, numberOfQuantiles, theoreticalQuantiles, session)

    theoreticalValues = [x/(numberOfQuantiles-1) for x in range(numberOfQuantiles)]
    t = (
        indicator.lower(),
        year,
        month,
        day,
        hour
    )
    query = """
            SELECT %s
            FROM weatherByTime
            WHERE DhmYear = %s AND DhmMonth = %s AND DhmDay = %s AND DhmHour = %s
            """
    nQ2 = 0
    results = session.execute(query, t)
    actualValues = [0] * numberOfQuantiles
    for row in results:
        nQ2 += 1
        for q in range(numberOfQuantiles) :
            if (row[0] > theoreticalQuantiles[q]) :
                actualValues[q] += 1
        if (row[0] > maxQ2):
            maxQ2 = row[0]
        if (row[0] < minQ2):
            minQ2 = row[0]


def getCumulativeDistributionFunctionQ2(indicator, hour, day, month, year, numberOfQuantiles) :
    if (not(checkIndicatorQ2(indicator))) :
        print("Indicateur ("+str(indicator)+") non autorisé.")
        return False

    t = (
        indicator.lower(),
        year,
        month,
        day,
        hour
    )
    cluster = Cluster(['localhost'])
    session = cluster.connect('dbermond_projet')

    query = """
        SELECT %s
        FROM weatherByTime
        WHERE DhmYear = %s AND DhmMonth = %s AND DhmDay = %s AND DhmHour = %s
        """
    results = session.execute(query, t)
    nQ2 = 0
    minQ2 = 99999999999999
    maxQ2 = -9999999999999
    for row in results:
        nQ2 += 1
        if (row[0] > maxQ2):
            maxQ2 = row[0]
        if (row[0] < minQ2):
            minQ2 = row[0]
    if (nQ2 == 0):
        print("Aucune donnée pour la date saisie (" + str(day) + "/" + str(month) + "/" + str(year) + ").")
        return False ;

    theoricalQuantiles = [minQ2 + (x/(numberOfQuantiles-1)) * (maxQ2 - minQ2) for x in range(numberOfQuantiles)]


****************************************************************************************************************** '''
